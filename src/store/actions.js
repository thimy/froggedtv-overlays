import axios from 'axios';

import * as types from './mutation-types';
import { OPENDOTA_API_URL, OPENDOTA_PROMATCHES, OPENDOTA_MATCH } from '../constants';

const actions = {
  [types.LOAD_MATCHES]({ commit }) {
    axios.get(OPENDOTA_API_URL + OPENDOTA_PROMATCHES)
      .then(res => commit(types.SET_MATCHES, res.data));
  },
  [types.LOAD_CURRENT_MATCH]({ state, commit }, { matchId }) {
    const currentMatchId = state.currentMatchId;
    if (!currentMatchId || currentMatchId !== matchId) {
      axios.get(OPENDOTA_API_URL + OPENDOTA_MATCH + matchId)
        .then(res => commit(types.SET_CURRENT_MATCH, res.data));
    }
  },
};

export default actions;
