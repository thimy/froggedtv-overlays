const getters = {
  lastMatches: state => state.lastMatches,
  tenLastMatches: state => state.lastMatches.slice(0, 10),
  currentMatch: state => state.currentMatch,
  currentMatchId: state => state.currentMatch.id,
  title: state => state.title,
  showModal: state => state.showModal,
};

export default getters;

