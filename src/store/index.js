import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

export const state = {
  currentMatch: Object,
  currentMatchId: null,
  lastMatches: [],
  title: String,
  showModal: false,
};

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
});
