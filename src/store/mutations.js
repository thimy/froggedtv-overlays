import * as types from './mutation-types';

const mutations = {
  [types.SET_MATCHES](state, matches) {
    state.lastMatches = matches;
  },
  [types.SET_CURRENT_MATCH](state, match) {
    state.currentMatch = match;
    state.currentMatchId = match.match_id;
    state.currentMatch.radiant = {
      name: match.radiant_team ? match.radiant_team.name : 'Radiant',
      players: match.players.filter(player => player.isRadiant),
    };
    state.currentMatch.dire = {
      name: match.dire_team ? match.dire_team.name : 'Radiant',
      players: match.players.filter(player => !player.isRadiant),
    };
  },
  [types.SET_TITLE](state, title) {
    state.title = title;
  },
  [types.SHOW_MODAL](state, showModal) {
    state.showModal = showModal;
  },
};

export default mutations;
