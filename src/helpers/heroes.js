import { OPENDOTA_API_HOST } from '../constants';
import heroes from '../../node_modules/dotaconstants/build/heroes.json';

function getHeroPortrait(heroId) {
  return OPENDOTA_API_HOST + heroes[heroId].img;
}

function getHeroName(heroId) {
  return OPENDOTA_API_HOST + heroes[heroId].name;
}

export {
  getHeroPortrait,
  getHeroName,
};
