import items from '../../node_modules/dotaconstants/build/items.json';
import { OPENDOTA_API_HOST } from '../constants';

function findById(itemId) {
  let item;
  const itemKeys = Object.keys(items);
  itemKeys.forEach((key) => {
    if (items[key].id === itemId) {
      item = items[key];
    }
  });
  return item;
}

function findItemImg(itemId) {
  const item = findById(itemId);
  return item ? OPENDOTA_API_HOST + item.img : '';
}

export {
  findById,
  findItemImg,
};
