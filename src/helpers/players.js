function getPlayerKda(player) {
  return `${player.kills} / ${player.deaths} / ${player.assists}`;
}

function getPlayerInventory(player) {
  return [player.item_0, player.item_1, player.item_2,
    player.item_3, player.item_4, player.item_5];
}

function getPlayerBackpack(player) {
  return [player.backpack_0, player.backpack_1, player.backpack_2];
}

export {
  getPlayerKda,
  getPlayerInventory,
  getPlayerBackpack,
};
