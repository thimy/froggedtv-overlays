export const OPENDOTA_API_HOST = 'https://api.opendota.com';
export const OPENDOTA_API_URL = 'https://api.opendota.com/api';
export const OPENDOTA_PROMATCHES = '/proMatches/';
export const OPENDOTA_MATCH = '/matches/';

