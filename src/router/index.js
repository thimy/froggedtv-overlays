import Vue from 'vue';
import Router from 'vue-router';
import Matches from '@/components/matches';
import Match from '@/components/match/match';
import Home from '../home';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    }, {
      path: '/matches',
      name: 'Matches',
      component: Matches,
    }, {
      path: '/match/:matchId',
      name: 'Match',
      component: Match,
    },
  ],
});
